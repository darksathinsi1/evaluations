# **Emploi du temps de DarkSATHI pour refaire les DS pendant votre temps libre et quand j'ai cours :**
+ [**SEMAINE A**](../semaineA.PNG)
+ [**SEMAINE B**](../semaineB.PNG)

-----------

# ![evaluation](evaluation2.png) [La conversion des entiers naturels](https://gitlab.com/darksathinsi1/evaluations/blob/master/evaluations/binairehexaEntierNaturels.pdf)
# ![evaluation](evaluation2.png) [Les bases du langage Python](evaluations/nsids1.pdf)
# ![evaluation](evaluation2.png) [QCM : Les listes](evaluations/nsids2.pdf)
# ![evaluation](evaluation2.png) [Bilan de la première période](evaluations/BilanNSI.pdf)

# **Deuxième Trimestre**
-----------

# ![evaluation](evaluation2.png) [Evaluation | Projet](evaluations/Evaluation.md)
# ![evaluation](evaluation2.png) [QCM Python Bilan](http://thierry-sautiere.000webhostapp.com/qcmsPythonNSI/)
