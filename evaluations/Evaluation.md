
# ![](images/kylo.png) **Evaluation : 2 heures**

## ![](images/anakin.png) **Exercice 1** (8 points)

**Pour chacune des séquences qui suivent, indiquez l’affichage qu’elle produit.**


```python
print(20 + int('17'))
```


```python
print (bin (37))
print (hex (37))
```


```python
print (0b0100101)
print (0x25)
```


```python
print (chr (ord ('B') + 5))
```


```python
i = 10
while i > 5:
    i = i - 2
    print (i)
```


```python
s = 'informatique'
print (s[2:6] + '-' + s[9:] + '**')
```


```python
def f(x):
    return x * 3 + 1
print (f(f(1)+1))
```


```python
a = [3, 1, 4, 1, 5]
b = 0
for i in range (len (a)):
    if ((i % 2) == 0) :
        b = b + a[i//2]
    else:
    b = b - a[i]
print (b)
```

## ![](images/anakin.png) **Exercice 2** (12 points)

Ce jeu consiste à deviner un mot en proposant des lettres une par une. Au départ, autant de - que de
lettres dans le mot à deviner sont affichées. 

Puis, les lettres devinées sont affichées dans le mot en cours de
construction. Il est toléré 6 erreurs, c’est-à-dire au maximum 6 lettres qui ne sont pas dans le mot. 

On définit la constante ```LISTE_MOTS``` qui est une liste de chaînes représentant des mots, en voici un extrait :


```python
LISTE_MOTS = ['Anakin', 'Luke', 'Palpatine', 'Leia', 'Rey', ...]
```

- **Question 1** : Écrire la fonction mot_au_hasard qui renvoie un mot choisi au hasard dans la liste ```LISTE_MOTS```.


```python
>>> mot_au_hasard()
'Palpatine'
```

- **Question 2** : Écrire la fonction cache_mot qui prend en paramètre une chaîne de caractères représentant un mot et qui renvoie une chaîne contenant autant de caractères - que la longueur de la chaîne passée en paramètre.


```python
>>> cache_mot('Palpatine')
'---------'
```

- **Question 3** : Écrire la fonction revele qui prend en paramètres une chaîne de caractères représentant le mot en cours de construction, le mot à deviner et une lettre l. Cette fonction renvoie une chaîne qui contient le mot en construction avec les lettres l affichées à la bonne place dans le mot en construction.


```python
>>> revele('---------', 'Palpatine', 'e')
'--------e'
>>> revele('--------e', 'Palpatine', 'a')
'-a--a---e'
>>> revele('---------', 'Palpatine', 'p')
'Pa-pa---e'
```

- **Question 4** : Écrire la fonction jeu qui permet de jouer au jeu du pendu. Cette fonction mémorise un mot choisi au hasard dans la liste LISTE_MOTS, affiche le mot en cours de construction et demande une lettre à l’utilisateur jusqu’à ce que le mot complet soit trouvé ou que 6 erreurs soient atteintes. Il y a une erreur lorsque la lettre proposée n’est pas présente dans le mot à deviner.

![](images/boba.jpg)
